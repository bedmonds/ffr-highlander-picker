(function(exports) {
// Quick and dirty assertion testing for FFR Flag Interop.

// Assertion counter.
var assertions = 0;

// Success counter
var successes = 0;

// Failure counter
var failures = 0;

// At-a-glance output for all tests run.
var testOutput = [];

// Error messages for failed tests.
var errors = [];

// Ensure that applying +settings+ correctly returns the
// +expected+ flag string.
//
// settings must be an array of option indices, with a number of elements 
//            equal to the number of option groups.
//
// expected is the flag string that should be returned by ApplySettings.
var $assert = function (expected, settings) {
    ++assertions;
    var got = FFRHighlander.ApplySettings(settings);

    if (expected === got) {
        ++successes;
        return [true, null];
    } else {
        ++failures;
        return [false, got];
    }
};

// Wrapper around an expected flag string and settings, with a
// human-friendly label.
var Test = function (label, expected, settings) {
    this.label = label;
    this.expected = expected;
    this.settings = settings;
}

// Run a single Test.
var RunTest = function (test) {
    var res = $assert(test.expected, test.settings);

    testOutput.push(res[0] ? '.' : 'F');

    if (!res[0]) {
       errors.push(
            '<p class="error"><strong>' +
            test.label +
            '</strong> failed<br><pre>    Expected: '
            + test.expected +
            "\n    Got:      " + res[1] + 
            '</pre></p>'
       );
    }
}

exports.Tests = {
    Test,
    Run: RunTest,
    GetCounters: () => {
        return {
            Assertions: assertions,
            Failures: failures,
            Successes: successes,
        };
    },
    stdout: testOutput,
    stderr: errors,
};
})(window);