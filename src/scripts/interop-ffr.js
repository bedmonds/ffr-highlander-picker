(function(exports){
const base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-!";
const defaultSettings = "DAABAEAAAAAADI!dPIKoKAe!GeK!!!AAEAAB";

var Flag = function (index, bit) {
    this.index = index;
    this.bit = bit;
};

var ScaleFlag = function (index, multiplier) {
    this.index = index;
    this.multiplier = multiplier;
};

const Flags = {
    Enemies: {
        AI: new Flag(4, 1),
        Spells: new Flag(4, 2),
        UnsafePirates: new Flag(4, 8),
        EnemyScale: new ScaleFlag(18, 0.1),
        BossScale: new ScaleFlag(26, 0.1),
    },

    Spells: {
        Levels: new Flag(2, 2),
        Shops: new Flag(2, 1),
        Permissions: new Flag(2, 4),
    },

    Yields: {
        Multiplier: new ScaleFlag(20, 0.1),
        Bonus: new ScaleFlag(21, 10),
    },

    WeaponFixes: new Flag(15, 2),

    Party: {
        Force: {
            Slot1: new Flag(30, 8),
            Slot2: new Flag(30, 16),
            Slot3: new Flag(30, 32),
            Slot4: new Flag(31, 1),
        },
        AllowNone: {
            Slot2: new Flag(30, 1),
            Slot3: new Flag(30, 2),
            Slot4: new Flag(30, 4),
        }
    },

    MapEdits: {
        OpenProgression: new Flag(6, 1),
        ExtendedOpen: new Flag(6, 32),
        EntranceShuffle: new Flag(6, 2),
        FloorShuffle: new Flag(6, 8),
        TownShuffle: new Flag(6, 4),

        ShortTOFR: new Flag(1, 16),
    },

    NPCItems: new Flag(0, 4),

    AlternateBoss: new Flag(1, 4),

    Goal: {
        ChaosRush: new Flag(1, 8),
        FreeOrbs: new Flag(13, 4),
        ShardHunt: new Flag(1, 1),
    }
};

var Modifier = function (flag, value) {
    this.flag = flag;
    this.value = value;
};

var Option = function (label, modifiers) {
    this.label = label;
    this.modifiers = modifiers;
};

var Setting = function (label, description, options) {
    this.label = label;
    this.description = description;
    this.options = options;
}

var $mkmod = (flag, value) => new Modifier(flag, value);

String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

const Settings = [
    // Enemies
    new Setting(
        'Enemies and Bosses',
        '', 
        [
            new Option('Vanilla', []),

            new Option('Fiendish', [
                $mkmod(Flags.Enemies.Spells, true),
                $mkmod(Flags.Enemies.EnemyScale, 5),
                $mkmod(Flags.Enemies.BossScale, 5),
            ]),

            new Option('Chaos', [
                $mkmod(Flags.Enemies.AI, true),
                $mkmod(Flags.Enemies.Spells, true),
                $mkmod(Flags.Enemies.UnsafePirates, true),
                $mkmod(Flags.Enemies.EnemyScale, 10),
                $mkmod(Flags.Enemies.BossScale, 10),
            ]),
        ]
    ),

    // Spells
    new Setting(
        'Spells',
        '',
        [
            new Option('Vanilla', []),

            new Option('Adept', [
                $mkmod(Flags.Spells.Levels, true),
            ]),

            new Option('Dobby', [
                $mkmod(Flags.Spells.Levels, true),
                $mkmod(Flags.Spells.Shops, true),
                $mkmod(Flags.Spells.Permissions, true),
            ]),
        ],
    ),

    new Setting(
        'Experience and Gold Yields',
        '',
        [
            new Option('Vanilla', []),

            new Option('GottaGoFast', [
                $mkmod(Flags.Yields.Multiplier, 15),
            ]),

            new Option('LOLXP', [
                $mkmod(Flags.Yields.Multiplier, 40),
                $mkmod(Flags.Yields.Bonus, 50),
            ]),
        ]
    ),

    new Setting(
        'Weapon Fixes',
        '',
        [
            new Option('Off', []),
            new Option('On', [
                $mkmod(Flags.WeaponFixes, true)
            ]),
        ]
    ),

    new Setting(
        'Party Composition',
        '',
        [
            new Option('Free form', [
                $mkmod(Flags.Party.AllowNone.Slot2, true),
                $mkmod(Flags.Party.AllowNone.Slot3, true),
                $mkmod(Flags.Party.AllowNone.Slot4, true),
            ]),

            new Option('Forced', [
                $mkmod(Flags.Party.Force.Slot1, true),
                $mkmod(Flags.Party.Force.Slot2, true),
                $mkmod(Flags.Party.Force.Slot3, true),
                $mkmod(Flags.Party.Force.Slot4, true),
            ]),

            new Option('Draft', []),
        ]
    ),

    new Setting(
        'Map and NPC Edits',
        '',
        [
            new Option('Vanilla', []),

            new Option('Extended', [
                $mkmod(Flags.MapEdits.OpenProgression, true),
                $mkmod(Flags.MapEdits.ExtendedOpen, true),
            ]),

            new Option('Helter Skelter', [
                $mkmod(Flags.NPCItems, true),
                $mkmod(Flags.MapEdits.EntranceShuffle, true),
                $mkmod(Flags.MapEdits.TownShuffle, true),
                $mkmod(Flags.MapEdits.FloorShuffle, true),
            ]),
        ]
    ),

    new Setting(
        'End Goal',
        '',
        [
            new Option('Vanilla', []),
            new Option('CHAOS Rush', [
                $mkmod(Flags.Goal.ChaosRush, true),
                $mkmod(Flags.Goal.FreeOrbs, true)
            ]),
            new Option('Biggses & Wedges', [
                $mkmod(Flags.Goal.ShardHunt, true),
                $mkmod(Flags.MapEdits.ShortTOFR, true),
                $mkmod(Flags.AlternateBoss, true),
            ]),
        ]
    ),
];

var ApplyModifier = (flags, modifier) => {
    const cur = flags.charAt(modifier.flag.index);
    let b64Index = base64Chars.indexOf(cur);

    switch(modifier.flag.constructor.name) {
        case 'Flag':
            if (modifier.value === true)
                b64Index += modifier.flag.bit;
        break;

        case 'ScaleFlag':
            b64Index += modifier.value;
        break;

        default:
            console.error('Unsupported flag type.');
            console.error(modifier);
            return;
    }

    return flags.replaceAt(modifier.flag.index, base64Chars[b64Index]);
};

var GenerateFlagString = (settings) => {
    if (settings.length < 2) { 
        console.error('Number of settings must match the number of flag pools');
        return;
    }

    let curFlags = defaultSettings;

    settings.forEach((i, idx) => {
        const option = Settings[idx].options[i];

        option.modifiers.forEach((mod) => {
            curFlags = ApplyModifier(curFlags, mod);
        });
    })
    
    return curFlags;
}

exports.FFRHighlander = {
    Settings,
    ApplySettings: GenerateFlagString
};
})(window);